# Toteem

Projet de sensibilisation aux handicap psychique dans l'entreprise pour réveiller l'empathie. 
Exposition immersive et sensible pour se mettre à la place des personnes atteintes de troubles psychiques.